﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PlaneCells"
{
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_Color1Cell("Cell Color1", Color) = (1,1,1,1)
		_Color2Cell("Cell Color2", Color) = (0,0,0,1)
		_RowsNumber("Rows number", int) = 8
	}

	SubShader {

		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				fixed4 _Color;
				fixed4 _Color1Cell;
				fixed4 _Color2Cell;
				int _RowsNumber;

				struct vertexInput {
					float4 vertex : POSITION;
					float4 texcoord0 : TEXCOORD0;
				};

				struct fragmentInput {
					float4 position : SV_POSITION;
					float4 texcoord0 : TEXCOORD0;
				};

				fragmentInput vert(vertexInput i) {
					fragmentInput o;
					o.position = UnityObjectToClipPos(i.vertex);
					o.texcoord0 = i.texcoord0;
					return o;
				}

				fixed4 frag(fragmentInput i) : SV_Target {

					fixed4 color;
					if (fmod(i.texcoord0.x*_RowsNumber,2.0) < 1.0) {
						if (fmod(i.texcoord0.y*_RowsNumber,2.0) < 1.0)
						{
							color = _Color1Cell;
						}
						else {
							color = _Color2Cell;
						}
					}
					else {
						if (fmod(i.texcoord0.y*_RowsNumber,2.0) > 1.0)
						{
							color = _Color1Cell;
						}
						else {
							color = _Color2Cell;
						}
					}

					return color * _Color;
				}
			ENDCG
		}
	}
}
