﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Moving : MonoBehaviour
{
    public Transform Plane;
    public float LookingResourcesDistance = 35;
    public float LookingBotsDistance = 40;
    public float LookingPlayerDistance = 30;
    public Vector3 Destination = Vector3.zero;

    private Vector3 _velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
        if (Plane == null)
        {
            var plane = GameObject.FindGameObjectWithTag("Plane");
            if (plane != null)
            {
                Plane = plane.transform;
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (!LookingDanger())
	    {
	        LookingDestination();
        }

	    var eatingScript = GetComponent<Eating>();
	    if (eatingScript != null)
	    {
            var smoothPosition = Vector3.SmoothDamp(transform.position, Destination, ref _velocity,
	            0.1f, eatingScript.Speed);
	        transform.position = smoothPosition;

	        var distance = Vector3.Distance(transform.position, Destination);
	        if (distance < 2)
	        {
	            LookingDestination();
            }
        }
	}

    private bool LookingDanger()
    {
        var bots = GameObject.FindGameObjectsWithTag("Bot");
        var player = GameObject.FindGameObjectWithTag("Player");

        // looking player
        if (player != null)
        {
            var playerDistance = Vector3.Distance(transform.position, player.transform.position);
            if (playerDistance < LookingPlayerDistance)
            {
                int iamEatsSecond = PlaneController2.FirstEatsSecond(transform, player.transform);
                if (iamEatsSecond == -1)
                {
                    return true;
                }
            }
        }

        // loking bots
        foreach (var bot in bots)
        {
            var d = Vector3.Distance(transform.position, bot.transform.position);
            if (d > LookingBotsDistance)
            {
                continue;
            }

            int iamEatsSecond = PlaneController2.FirstEatsSecond(transform, bot.transform);
            if (iamEatsSecond == -1)
            {
                return true;
            }
        }

        return false;
    }

    private void LookingDestination()
    {
        var planeBounds = Plane.GetComponent<Collider>().bounds;

        // looking for nearlier resources
        List<GameObject> objects = new List<GameObject>();
        objects.AddRange(GameObject.FindGameObjectsWithTag("Resource"));
        objects.AddRange(GameObject.FindGameObjectsWithTag("Bot"));
        objects.Add(GameObject.FindGameObjectWithTag("Player"));

        List<GameObject> found = new List<GameObject>();
        foreach (var o in objects)
        {
            var distance = Vector3.Distance(transform.position, o.transform.position);
            if (o.tag.Contains("Player") && distance <= LookingPlayerDistance)
            {
                if (PlaneController2.FirstEatsSecond(transform, o.transform) == 1)
                {
                    found.Add(o);
                }
            }
            else if (o.tag.Contains("Bot") && distance <= LookingBotsDistance)
            {
                if (PlaneController2.FirstEatsSecond(transform, o.transform) == 1)
                {
                    found.Add(o);
                }
            }
            else if (o.tag.Contains("Resource") && distance <= LookingResourcesDistance)
            {
                if (PlaneController2.FirstEatsSecond(transform, o.transform) == 1)
                {
                    found.Add(o);
                }
            }
        }

        if (found.Count > 0)
        {
            // looking very fat object
            var fat = found[0];
            var capacity = PlaneController2.GetCapacity(fat.transform);
            foreach (var o in found)
            {
                var c = PlaneController2.GetCapacity(o.transform);
                if (c > capacity)
                {
                    capacity = c;
                    fat = o;
                }
            }

            Destination = fat.transform.position;
        }
        else
        {
            // random destinaion
            var distance = Vector3.Distance(transform.position, Destination);
            if (distance < 2)
            {
                // random destinaion
                var randomDestination =
                    new Vector3(Random.Range(-planeBounds.size.x / 2, planeBounds.size.x / 2), transform.position.y,
                        Random.Range(-planeBounds.size.z / 2, planeBounds.size.z / 2));

                Destination = randomDestination;
            }
        }
    }
}
