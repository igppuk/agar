﻿using System.Collections;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public float MinShootingCapacity = 20.0f;
    public float ShootingSpeed = 6.0f;

    private Vector3 _velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Fire(Transform o, Vector3 destination)
    {
        if (o.parent == null)
            return;

        var capacity = PlaneController2.GetCapacity(o);
        if (capacity < MinShootingCapacity)
            return;

        var clone = Split(o.gameObject);
        var maxDistance = clone.transform.localScale.x * 1.3f;

        StartCoroutine(ReduceObject(o, clone.transform.localScale, 0.2f));

        // shoot child from parent
        var childPosition = Vector3.ClampMagnitude(destination, maxDistance);
        StartCoroutine(MoveForward(clone.transform, childPosition, 0.2f));

        var movingScript = o.parent.GetComponent<MovingController>();
        if (movingScript != null)
        {
            movingScript.Splitted = true;
        }
    }

    public GameObject Split(GameObject o)
    {
        GameObject clone = Instantiate(o, o.transform.parent, false);
        clone.transform.localScale = new Vector3(o.transform.localScale.x / 2, o.transform.localScale.y, o.transform.localScale.z / 2);

        return clone;
    }

    private IEnumerator ReduceObject(Transform o, Vector3 toLocalScale, float time)
    {
        const int steps = 8;
        var scaleDiff = (o.localScale - toLocalScale) / steps;
        var timeDiff = time / steps;

        for (int j = 0; j < steps; j++)
        {
            o.localScale -= scaleDiff;
            yield return new WaitForSeconds(timeDiff);
        }
    }

    private IEnumerator MoveForward(Transform o, Vector3 position, float time)
    {
        const int steps = 8;
        var timeDiff = time / steps;

        for (int i = 0; i < steps; i++)
        {
            var smoothPosition = Vector3.SmoothDamp(o.localPosition, position, ref _velocity, 0.1f);
            o.localPosition = smoothPosition;
            yield return new WaitForSeconds(timeDiff);
        }
    }
}
