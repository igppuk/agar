﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingController : MonoBehaviour {

    public Vector3 Destination = Vector3.zero;
    public float SplittingDuration = 20.0f;
    public Transform Plane;

    public bool Splitted
    {
        get { return _splitted; }
        set
        {
            _splitted = value;
            _splitTime = Time.time;
        }
    }

    private Vector3 _velocity1 = Vector3.zero;
    private Vector3 _velocity2 = Vector3.zero;
    private bool _splitted;
    private float _splitTime;

    // Update is called once per frame
    void Update () {
        if (Plane != null)
        {
            var planeController = Plane.GetComponent<PlaneController2>();
            if (!planeController.AllowPlayerMove)
            {
                return;
            }
        }

	    var distance = Vector3.Distance(Destination, transform.position);
	    if (distance > 1)
	    {
            // move parent to destination

	        var childCount = transform.childCount;
	        if (childCount > 0)
	        {
	            var eatingScript = transform.GetChild(0).GetComponent<Eating>();
                var smoothPosition = Vector3.SmoothDamp(transform.position, Destination, ref _velocity1, 0.1f, eatingScript.Speed);
	            transform.position = smoothPosition;

	            Camera.main.transform.position = new Vector3(smoothPosition.x, Camera.main.transform.position.y, smoothPosition.z);
            }
	    }

        if (Splitted)
        {
            if (Time.time >= _splitTime + SplittingDuration)
            {
                Splitted = false;

                // connect all childrens in one object
                var totalCapacity = 0.0f;
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i);
                    var childCapacity = PlaneController2.GetCapacity(child);
                    totalCapacity += childCapacity;

                    if (i > 0)
                    {
                        Destroy(child.gameObject);
                    }
                }

                var toScale = new Vector3(totalCapacity/transform.GetChild(0).localScale.x, transform.GetChild(0).localScale.y, totalCapacity/transform.GetChild(0).localScale.z);
                StartCoroutine(ExpandObject(transform.GetChild(0).transform, toScale, 0.4f));
            }
        }
    }

    private IEnumerator ExpandObject(Transform o, Vector3 toLocalScale, float time)
    {
        const int steps = 8;
        var scaleDiff = toLocalScale / steps;
        var timeDiff = time / steps;

        for (int j = 0; j < steps; j++)
        {
            o.localScale += scaleDiff;
            yield return new WaitForSeconds(timeDiff);
        }
    }
}
