﻿using System.Collections;
using UnityEngine;

public class Eating : MonoBehaviour
{
    public Transform Plane;
    public float Speed = 10;
    public float MaxSpeed = 10;
    public float MaxCapacity = 100;

    private bool _exploding;

	// Use this for initialization
	void Start () {

	    if (Plane == null)
	    {
	        Plane = GameObject.FindGameObjectWithTag("Plane").transform;
	    }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag.Contains("Plane"))
            return;

        var distance = Vector3.Distance(transform.position, other.transform.position);
        var playerR = transform.localScale.x / 2;

        if (distance < playerR)
        {
            var planeScript = Plane.GetComponent<PlaneController2>();
            int iamEatsSecond = PlaneController2.FirstEatsSecond(transform, other.transform);
            float ration = PlaneController2.GetRation(other.transform, transform);

            bool shouldDestroy = false;
            var capacity = PlaneController2.GetCapacity(transform);
            if (other.tag.Contains("Resource"))
            {
                StartCoroutine(AnimateEating(ration, 0, ration, 0.5f));
                shouldDestroy = true;
                planeScript.AddResourceTime(Time.time, other.transform.position);
            }
            else if (other.tag.Contains("Bot"))
            {
                if (iamEatsSecond == 1)
                {
                    StartCoroutine(AnimateEating(ration, 0, ration, 0.2f));
                    planeScript.AddBotTime(Time.time, other.transform.position);
                    shouldDestroy = true;
                }
            }
            else if (other.tag.Contains("Player"))
            {
                if (!tag.Contains("Player"))
                {
                    if (iamEatsSecond == 1)
                    {
                        StartCoroutine(AnimateEating(ration, 0, ration, 0.2f));

                        // game over
                        planeScript.GameOver();
                        return;
                    }
                }
            }
            else if (other.tag.Contains("Bomb"))
            {
                if (capacity > MaxCapacity / 7 && !_exploding)
                {
                    StartCoroutine(AnimateExploding(2f, 0, 2f, 0.2f));
                }
            }

            var capacityRation = capacity * 100 / MaxCapacity;
            var speed = MaxSpeed - (MaxSpeed * capacityRation / 100);

            var eatingScript = GetComponent<Eating>();
            if (eatingScript != null)
            {
                // change the speed
                if (speed <= 1.2 && speed >= 0.8)
                {
                    eatingScript.Speed = 1.2f;
                }
                else if (speed < 0.8 && speed >= 0.5)
                {
                    eatingScript.Speed = 0.7f;
                }
                else if (speed < 0.5)
                {
                    eatingScript.Speed = 0.5f;
                }
                else
                {
                    eatingScript.Speed = speed;
                }

                var movingScript = GetComponent<Moving>();
                if (movingScript != null)
                {
                    if (speed < MaxSpeed * 0.7)
                    {
                        movingScript.LookingBotsDistance = 30;
                        movingScript.LookingPlayerDistance = 27;
                    }
                    else if (speed < MaxSpeed * 0.5)
                    {
                        movingScript.LookingBotsDistance = 27;
                        movingScript.LookingPlayerDistance = 25;
                    }
                    else if (speed < MaxSpeed * 0.4)
                    {
                        movingScript.LookingBotsDistance = 20;
                        movingScript.LookingPlayerDistance = 20;
                    }
                }

                //Debug.Log(eatingScript.Speed);
            }

            if (shouldDestroy)
            {
                planeScript.AddResourceTime(Time.time, other.transform.position);
                Destroy(other.gameObject);
            }
        }
    }

    IEnumerator AnimateEating(float scaleX, float scaleY, float scaleZ, float time)
    {
        // in x steps
        const int steps = 8;

        float diffX = scaleX / steps;
        float diffY = scaleY / steps;
        float diffZ = scaleZ / steps;

        for (int i = 0; i < steps; i++)
        {
            transform.localScale += new Vector3(diffX, diffY, diffZ);
            yield return new WaitForSeconds(time/steps);
        }
    }

    IEnumerator AnimateExploding(float scaleX, float scaleY, float scaleZ, float time)
    {
        _exploding = true;

        // in x steps
        const int steps = 8;

        float diffX = scaleX / steps;
        float diffY = scaleY / steps;
        float diffZ = scaleZ / steps;

        for (int i = 0; i < steps; i++)
        {
            transform.localScale -= new Vector3(diffX, diffY, diffZ);
            yield return new WaitForSeconds(time / steps);
        }

        _exploding = false;
    }
}
