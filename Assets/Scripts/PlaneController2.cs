﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PlaneController2 : MonoBehaviour
{

    public Transform Player;
    public bool AllowPlayerMove = true;
    public GameObject ResourcePrefab;
    public GameObject BotPrefad;
    public GameObject BombPrefab;
    public int MaxResourceNumber = 500;
    public int MaxBotNumber = 8;
    public int MaxBombsNumber = 20;
    public float WaitingToRestoreResource = 100;
    public float WaitingToRestoreBot = 10;

    public Text Place1Text;
    public Text Place2Text;
    public Text Place3Text;
    public Text Place4Text;
    public Text Place5Text;
    public Text Place6Text;
    public Text Place7Text;
    public Text Place8Text;
    public Text Place9Text;

    private Vector3 _playerDestination = Vector3.zero;
    private bool _notUpdate;
    private int _botCounter;
    private bool _showFullScreen;

    private readonly Dictionary<float, Vector3> _resourceTimes = new Dictionary<float, Vector3>();
    private readonly Dictionary<float, Vector3> _botTimes = new Dictionary<float, Vector3>();

    // Use this for initialization
    void Start()
    {
        Player.name = "YOU";

        DrawResources();
        DrawBots();
        DrawBombs();
    }

    // Update is called once per frame
    void Update()
    {
        if (_notUpdate)
            return;

        if (Player != null && AllowPlayerMove)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000))
            {
                _playerDestination = new Vector3(hit.point.x, Player.position.y, hit.point.z);
            }

            var movingScript = Player.transform.parent.GetComponent<MovingController>();
            movingScript.Destination = _playerDestination;

            // check shooting
            if (!_showFullScreen)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    var shootingScript = GetComponent<Shooting>();
                    if (shootingScript != null)
                    {
                        shootingScript.Fire(Player, _playerDestination);
                    }
                }
            }
        }

        // check destroy times and return resources if necessary
        List<float> toDelete = new List<float>();
        foreach (KeyValuePair<float, Vector3> pair in _resourceTimes)
        {
            if (Time.time - pair.Key >= WaitingToRestoreResource)
            {
                toDelete.Add(pair.Key);
                CreateResource(pair.Value, Random.ColorHSV());
            }
        }

        foreach (float key in toDelete)
        {
            _resourceTimes.Remove(key);
        }

        toDelete.Clear();
        foreach (KeyValuePair<float, Vector3> pair in _botTimes)
        {
            if (Time.time - pair.Key >= WaitingToRestoreBot)
            {
                toDelete.Add(pair.Key);
                CreateBot(pair.Value, Random.ColorHSV());
            }
        }

        foreach (float key in toDelete)
        {
            _botTimes.Remove(key);
        }

        // update score
        UpdateScore();

        // check esc key
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _showFullScreen = !_showFullScreen;
            if (_showFullScreen)
            {
                AllowPlayerMove = false;
                Camera.main.fieldOfView = 130;
            }
            else
            {
                AllowPlayerMove = true;
                Camera.main.fieldOfView = 47;
            }
        }
    }

    void OnMouseDown()
    {
        AllowPlayerMove = !AllowPlayerMove;
    }

    private void DrawResources()
    {
        // create random resources
        var resourcesCount = GameObject.FindGameObjectsWithTag("Resource").Length;
        var cellsCount = (int) Mathf.Sqrt(MaxResourceNumber - resourcesCount);

        var horSize = GetComponent<Collider>().bounds.size.x;
        var verSize = GetComponent<Collider>().bounds.size.z;

        var horDiff = horSize / cellsCount;
        var verDiff = verSize / cellsCount;

        for (float x = -horSize / 2; x <= horSize / 2 - horDiff; x += horDiff)
        {
            for (float z = -verSize / 2; z <= verSize / 2 - verDiff; z += verDiff)
            {
                var randomX = Random.Range(x, x + horDiff);
                var randomZ = Random.Range(z, z + verDiff);
                CreateResource(new Vector3(randomX, Player.position.y, randomZ), Random.ColorHSV());
            }
        }
    }

    private void DrawBots()
    {
        // create random bots
        var botsCount = GameObject.FindGameObjectsWithTag("Bot").Length;
        var cellsCount = Mathf.Sqrt(MaxBotNumber - botsCount);

        var horSize = GetComponent<Collider>().bounds.size.x;
        var verSize = GetComponent<Collider>().bounds.size.z;

        var horDiff = horSize / cellsCount;
        var verDiff = verSize / cellsCount;

        for (float x = -horSize / 2; x <= horSize / 2; x += horDiff)
        {
            for (float z = -verSize / 2; z <= verSize / 2; z += verDiff)
            {
                var randomX = Random.Range(x, x + horDiff);
                var randomZ = Random.Range(z, z + verDiff);
                CreateBot(new Vector3(randomX, Player.position.y, randomZ), Random.ColorHSV());
            }
        }
    }

    private void DrawBombs()
    {
        // create random bots
        var cellsCount = Mathf.Sqrt(MaxBombsNumber);

        var horSize = GetComponent<Collider>().bounds.size.x;
        var verSize = GetComponent<Collider>().bounds.size.z;

        var horDiff = horSize / cellsCount;
        var verDiff = verSize / cellsCount;

        for (float x = -horSize / 2; x <= horSize / 2 - horDiff; x += horDiff)
        {
            for (float z = -verSize / 2; z <= verSize / 2 - verDiff; z += verDiff)
            {
                var randomX = Random.Range(x, x + horDiff);
                var randomZ = Random.Range(z, z + verDiff);
                CreateBomb(new Vector3(randomX, Player.position.y - 0.5f, randomZ), Random.ColorHSV());
            }
        }
    }

    private void CreateResource(Vector3 position, Color color)
    {
        GameObject resource = Instantiate(ResourcePrefab) as GameObject;
        resource.transform.position = position;
        resource.GetComponent<Renderer>().material.color = color;
    }

    private void CreateBot(Vector3 position, Color color)
    {
        GameObject bot = Instantiate(BotPrefad) as GameObject;
        bot.name = String.Format("{0}", _botCounter++);
        bot.transform.position = position;
        bot.GetComponent<Renderer>().material.color = color;
    }

    private void CreateBomb(Vector3 position, Color color)
    {
        GameObject bomb = Instantiate(BombPrefab) as GameObject;
        bomb.transform.position = position;
        //resource.GetComponent<Renderer>().material.color = color;
    }

    private void UpdateScore()
    {
        var bots = GameObject.FindGameObjectsWithTag("Bot");
        Dictionary<float, GameObject> dBot = new Dictionary<float, GameObject>();

        foreach (var bot in bots)
        {
            float capacity = GetCapacity(bot.transform);
            if (!dBot.ContainsKey(capacity))
            {
                dBot.Add(capacity, bot);
            }
        }

        var playerCapacity = GetCapacity(Player);
        if (!dBot.ContainsKey(playerCapacity))
        {
            dBot.Add(playerCapacity, Player.gameObject);
        }

        int index = 0;
        bool isPlayer = false;
        foreach (var bot in dBot.OrderByDescending(d => d.Key))
        {
            if (index > 9)
            {
                break;
            }

            if (bot.Value.tag.Contains("Player"))
            {
                isPlayer = true;
            }

            switch (index)
            {
                case 0:
                    Place1Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 1:
                    Place2Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 2:
                    Place3Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 3:
                    Place4Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 4:
                    Place5Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 5:
                    Place6Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 6:
                    Place7Text.text = String.Format("Bot '{0}' ({1:0.00})", bot.Value.name, bot.Key);
                    break;
                case 7:
                    Place8Text.text = "..................";
                    break;
                case 8:
                    var minBot = dBot.OrderByDescending(d => d.Key).Last();
                    Place9Text.text = !isPlayer ? String.Format("Bot 'YOU' ({0:0.00})", playerCapacity) : String.Format("Bot '{0}' ({1:0.00})", minBot.Value.name, minBot.Key);
                    break;
            }

            index += 1;
        }
    }

    public void AddResourceTime(float time, Vector3 position)
    {
        if (!_resourceTimes.ContainsKey(time))
        {
            _resourceTimes.Add(time, position);
        }
    }

    public void AddBotTime(float time, Vector3 position)
    {
        if (!_botTimes.ContainsKey(time))
        {
            _botTimes.Add(time, position);
        }
    }

    public void GameOver()
    {
        _notUpdate = true;

        var resources = GameObject.FindGameObjectsWithTag("Resource");
        var bots = GameObject.FindGameObjectsWithTag("Bot");
        var bombs = GameObject.FindGameObjectsWithTag("Bomb");

        _resourceTimes.Clear();
        _botTimes.Clear();

        foreach (var resource in resources)
        {
            Destroy(resource);
        }

        foreach (var bot in bots)
        {
            Destroy(bot);
        }

        foreach (var bomb in bombs)
        {
            Destroy(bomb);
        }

        Invoke("DrawResources", 2);
        Invoke("DrawBots", 3);
        Invoke("DrawBombs", 5);

        Player.localScale = new Vector3(2.0f, 0.4f, 2.0f);

        _notUpdate = false;
    }

    public static int FirstEatsSecond(Transform first, Transform second)
    {
        float firstCapacity = first.transform.localScale.x * first.transform.localScale.y * first.transform.localScale.z;
        float secondCapacity = second.localScale.x * second.localScale.y * second.localScale.z;
        float ration = firstCapacity / secondCapacity;

        if (ration > 1.5)
        {
            return 1;
        }

        if (ration < 0.5)
        {
            return -1;
        }

        return 0;
    }

    public static float GetRation(Transform first, Transform second)
    {
        float firstCapacity = first.transform.localScale.x * first.transform.localScale.y * first.transform.localScale.z;
        float secondCapacity = second.localScale.x * second.localScale.y * second.localScale.z;
        float ration = firstCapacity / secondCapacity;

        return ration;
    }

    public static float GetCapacity(Transform o)
    {
        float capacity = o.localScale.x * o.localScale.y * o.localScale.z;
        return capacity;
    }
}
